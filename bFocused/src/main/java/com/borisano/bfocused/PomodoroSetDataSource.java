package com.borisano.bfocused;

import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.w3c.dom.Comment;

public class PomodoroSetDataSource {

    // Database fields
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;

    private String[] allColumns = {
            MySQLiteHelper.COLUMN_ID,
            MySQLiteHelper.COLUMN_NAME,
            MySQLiteHelper.COLUMN_NUM_OF_POMODOROS
    };

    public PomodoroSetDataSource(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public PomodoroSet createPomodoroSet(String name) {
        return this.createPomodoroSet( name, 0 );
    }

    public PomodoroSet createPomodoroSet(String name, int numOfPomodoros) {
        return this.createPomodoroSet( name, "" + numOfPomodoros );
    }

    public PomodoroSet createPomodoroSet(String name, String numOfPomodoros) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_NAME, name);
        values.put(MySQLiteHelper.COLUMN_NUM_OF_POMODOROS, numOfPomodoros);

        long insertId = database.insert(MySQLiteHelper.TABLE_POMODOROS, null,
                values);
        Cursor cursor = database.query(MySQLiteHelper.TABLE_POMODOROS,
                allColumns, MySQLiteHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        PomodoroSet newPomodoroSet = cursorToPomodoroSet(cursor);
        cursor.close();
        return newPomodoroSet;
    }

    public void deletePomodoroSet(PomodoroSet ps) {
        long id = ps.id;
        Log.d("bboo", "Pomodoroset deleted with id: " + id);
        database.delete(MySQLiteHelper.TABLE_POMODOROS, MySQLiteHelper.COLUMN_ID
                + " = " + id, null);
    }

    public void deleteAllPomodoroSets() {
        database.delete( MySQLiteHelper.TABLE_POMODOROS ,null,null);
    }

    public ArrayList<PomodoroSet> getAllPomodoroSets() {
        ArrayList<PomodoroSet> pomodoroSets = new ArrayList<PomodoroSet>();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_POMODOROS,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PomodoroSet ps = cursorToPomodoroSet(cursor);
            pomodoroSets.add(ps);
            cursor.moveToNext();
        }
        // Make sure to close the cursor
        cursor.close();
        return pomodoroSets;
    }

    private PomodoroSet cursorToPomodoroSet(Cursor cursor) {
        PomodoroSet ps = new PomodoroSet( cursor.getString(1), Integer.parseInt( cursor.getString(2) ) );
//        ps.setId(cursor.getLong(0));
//        ps.setComment(cursor.getString(1));
        return ps;
    }
}