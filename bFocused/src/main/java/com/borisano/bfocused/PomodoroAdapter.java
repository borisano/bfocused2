package com.borisano.bfocused;


import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.app.Activity;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PomodoroAdapter extends BaseAdapter {

    Context context;
    LayoutInflater lInflater;
    ArrayList<PomodoroSet> objects;

    PomodoroAdapter( Context ctx, ArrayList<PomodoroSet> pomodoros ) {
        this.context = ctx;
        this.objects = pomodoros;
        this.lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setPomodoroSetName( int position, String newName ) {
        PomodoroSet pomodoro = this.getPomodoroSet(position);
        pomodoro.name = newName;
    }

    public PomodoroSet addItem( String name ) {
        PomodoroSet newPomodoro = new PomodoroSet(
                name,
                0
        );
        this.objects.add(newPomodoro);

        return newPomodoro;
    }

    public void deleteItem( int position) {
        this.objects.remove( position );
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    PomodoroSet getPomodoroSet(int position) {
        return ((PomodoroSet) getItem(position));
    }


    //pomodoroset item
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // используем созданные, но не используемые view
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.item, parent, false);
        }

        PomodoroSet ps = getPomodoroSet(position);

        // заполняем View в пункте списка данными из товаров: наименование, цена
        // и картинка
        TextView setNameTextView = (TextView) view.findViewById(R.id.setName);
        setNameTextView.setText( ps.name );

        LinearLayout pomodorosHolder = (LinearLayout) view.findViewById( R.id.pomodorosHolder );
        LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(80,80);

        pomodorosHolder.removeAllViews();
        for( int i = 0; i < ps.numOfPomodoros; i++ ) {

            ImageView pomodoroImage = new ImageView(context); // (ImageView) view.findViewById( R.id.pomodoroImage );
            pomodoroImage.setBackgroundResource(R.drawable.tomato);
            pomodorosHolder.addView(pomodoroImage, lParams);
        }

        return view;
    }


}