package com.borisano.bfocused;

import android.content.Context;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.util.Log;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

public class PomodoroTimer extends CountDownTimer {

    private TextView timerField;
    private PomodoroAdapter pAdapter;
    private PomodoroSet pomodoroSet;
    private PomodoroSwitchable endable;

    public boolean justStop = false;


    public PomodoroTimer( long pomodoroTime, TextView timerField, PomodoroAdapter pAdapter, int position, PomodoroSwitchable endable) {
        super( pomodoroTime, 1000);

        this.timerField = timerField;
        this.pAdapter = pAdapter;
        this.pomodoroSet = (PomodoroSet)  this.pAdapter.getItem(position);
        this.endable = endable;
        this.justStop = false;

    }

    @Override
    public void onTick(long msFinished) {
        timerField.setText(  msToMinutes(msFinished) );
    }

    @Override
    public void onFinish() {
        timerField.setText("Done");
        endable.setPomodoroRunning(false);
        if(justStop ) {
            return;
        }

        pomodoroSet.numOfPomodoros++;
        pAdapter.notifyDataSetChanged();
        justStop = true;


    }


    public String msToMinutes( long timeInMs ) {
        String formattedTime = String.format("%d:%d",
                TimeUnit.MILLISECONDS.toMinutes(timeInMs),
                TimeUnit.MILLISECONDS.toSeconds(timeInMs) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timeInMs))
        );
        return formattedTime;
    }
}
