package com.borisano.bfocused;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.app.Activity;
import android.os.Handler;
import android.os.Vibrator;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.*;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity implements OnItemClickListener, PomodoroSwitchable, AdapterView.OnItemLongClickListener, View.OnClickListener {

    public static Activity thisActivity;

    final int MENU_ADD_TASK = 1;
    final long POMODORO_TIME_MS = 25/25 * 60/6 * 1000;

    final SwipeDetector swipeDetector = new SwipeDetector();

    public Boolean isPomodoroRunning = false;

    Button stopPomodoro;

    ListView lvSimple;
    TextView timer;

    PomodoroAdapter pAdapter;

    PomodoroTimer pomodoroTimer;

    PomodoroSetDataSource datasource;

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0,MENU_ADD_TASK,0,"Add new task");

        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_ADD_TASK:
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

                View v = this.getLayoutInflater().inflate(R.layout.set_pomodoro_name, null);
                alertDialog.setView(v);

                final EditText pomodoroSetName = (EditText) v.findViewById(R.id.pomodoroSetName);
                alertDialog.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                alertDialog.setPositiveButton("Add",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String newPomodoroName = pomodoroSetName.getText().toString();
                                pAdapter.addItem( newPomodoroName );
                                pAdapter.notifyDataSetChanged();
                            }
                        });

                alertDialog.setMessage("Set new pomodoro name");
                alertDialog.setTitle("New pomodoro set");
                alertDialog.create();  //will show dialog
                alertDialog.show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        datasource = new PomodoroSetDataSource(this);
        datasource.open();

        ArrayList<PomodoroSet> pomodoros = datasource.getAllPomodoroSets();

        pAdapter = new PomodoroAdapter(this, pomodoros);
        datasource.deleteAllPomodoroSets();

        stopPomodoro = (Button) this.findViewById( R.id.stopTimer );
        stopPomodoro.setOnClickListener(this);

        // определяем список и присваиваем ему адаптер
        lvSimple = (ListView) findViewById(R.id.lvSimple);

        lvSimple.setAdapter(pAdapter);

        lvSimple.setOnTouchListener(swipeDetector);
        lvSimple.setOnItemClickListener(this);
        lvSimple.setOnItemLongClickListener( this );

    }

    @Override
    public void onPause() {
        super.onPause();

        for (PomodoroSet ps : pAdapter.objects) {
            datasource.createPomodoroSet( ps.name, ps.numOfPomodoros );
        }

        datasource.close();
    }

    @Override
    public void onResume() {
        super.onResume();

        datasource.open();
    }


    public void setPomodoroRunning( boolean status ) {
        this.isPomodoroRunning = status;
        if( this.isPomodoroRunning == false){
            this.pomodoroTimer.cancel();
            this.pomodoroTimer = null;

            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(2000);
        }

        stopPomodoro.setEnabled( status );
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
        if (swipeDetector.swipeDetected()){
            // do the onSwipe action
            Log.d("bboo","swipe on itemClick");

            Animation anim = AnimationUtils.loadAnimation(
                    this, android.R.anim.fade_out
            );
            anim.setDuration(500);
            parent.getChildAt(position).startAnimation(anim);

            new Handler().postDelayed(new Runnable() {

                public void run() {

                    pAdapter.deleteItem( position );
                    pAdapter.notifyDataSetChanged();
                }

            }, anim.getDuration());


        } else {
            Log.d("bboo","click on itemClick");
            if( this.isPomodoroRunning ) {
                return;
            }

            timer = (TextView) findViewById( R.id.timer );
            pomodoroTimer = new PomodoroTimer( this.POMODORO_TIME_MS, timer, pAdapter, position, this);

            this.setPomodoroRunning(true);
            pomodoroTimer.start();

            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            // Vibrate for 500 milliseconds
            v.vibrate(500);
        }

    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
        if ( !swipeDetector.swipeDetected()){
            // do the onItemLongClick action
            this.editPomodoroSet(pAdapter, position);

        }
        return true;
    }

    private void editPomodoroSet( final PomodoroAdapter pAdapter, final int position ) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        View v = this.getLayoutInflater().inflate(R.layout.set_pomodoro_name, null);
        alertDialog.setView(v);

        alertDialog.setCancelable(false);
        final EditText pomodoroSetName = (EditText) v.findViewById(R.id.pomodoroSetName);
        pomodoroSetName.setText( pAdapter.getPomodoroSet(position).name );
        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        alertDialog.setPositiveButton("Save",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String newPomodoroName = pomodoroSetName.getText().toString();
                        pAdapter.setPomodoroSetName(position, newPomodoroName);
                        pAdapter.notifyDataSetChanged();
                    }
                });

        alertDialog.setMessage("Set new pomodoro name");
        alertDialog.setTitle("Pomodoro edit");
        alertDialog.create();  //will show dialog
        alertDialog.show();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.stopTimer:
                pomodoroTimer.justStop = true;
                pomodoroTimer.onFinish();
            break;
        }
    }

}