package com.borisano.bfocused;

/**
 * Created by borisano on 13.06.13.
 */
public interface PomodoroSwitchable {
    public void setPomodoroRunning(boolean status);
}
